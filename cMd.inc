/************************************************************************************\
**																					**
**  cMd v0.1a (also called "mcmd") by Meta. (Capital "M" because of "Meta")			**
**  Copyright 2011 Meta (http://meta.spitnex.de/?lang=en). All rights reserved.		**
**																					**
**  Time in tests (no liez!): around 2-4 ms! (can't believe it myself)				**
**  And yes, the uncommented 3 lines below (depends on "benchmark" definition) are 	**
**	the FULL code! (instead of sscanf by Y_less)									**
**                                                  								**
**************************************************************************************
**                                                                  				**
**						=== TUTORIAL ===											**
**                                                                  				**
**	- Use "do_cMd;" in "OnPlayerCommandText" to call the command like that:			**
**                                                                  				**
**	public OnPlayerCommandText(playerid, cmdtext[])									**
**	{                                                               				**
**		// code ...                                                 				**
**      do_cMd;                                                            			**
**		// code ...                                                 				**
**	}                                                                           	**
**                                                                  				**
**                                                                  				**
**	- Define Commands like that:                                    				**
**                                                                  				**
**	cMd:CommandName;                                                				**
**	{                                                               				**
**		// code ...                                                 				**
**	}                                                               				**
**                                                                  				**
**    ... as a standalone function like public or stock.              				**
**                                                                  				**
**  - playerid, params[] and opt[] are available in every command, params[] 	 	**
**    includes the command's parameters you can split with sscanf, opt[] is useful	**
**    for optional parameters or just to be used as string.            				**
**                                                                  				**
\************************************************************************************/

//#define benchmark

#pragma tabsize 0

#if !defined benchmark
	#define do_cMd; \
     new cMdtMp[2][128],cMd;sscanf(cmdtext[1],"ss",cMdtMp[0],cMdtMp[1]);for(new v_cMd;v_cMd<strlen(cMdtMp[0]);v_cMd++){cMdtMp[0][v_cMd]=tolower(cMdtMp[0][v_cMd]);}if(!strlen(cMdtMp[1])){cMdtMp[1]="a";} \
		format(cmdtext,32,"cMd_%s",cMdtMp[0]);if(funcidx(cmdtext)!=-1&&!cMd){cMd=1;CallLocalFunction(cmdtext,"dss",playerid,cMdtMp[1],"a");}
#else
	#define do_cMd; \
	    new cMdtMp[2][128],cMd,cMd_time=GetTickCount();sscanf(cmdtext[1],"ss",cMdtMp[0],cMdtMp[1]);for(new v_cMd;v_cMd<strlen(cMdtMp[0]);v_cMd++){cMdtMp[0][v_cMd]=tolower(cMdtMp[0][v_cMd]);}if(!strlen(cMdtMp[1])){cMdtMp[1]="a";} \
		format(cmdtext,32,"cMd_%s",cMdtMp[0]);if(funcidx(cmdtext)!=-1&&!cMd){cMd=1;CallLocalFunction(cmdtext,"dss",playerid,cMdtMp[1],"a");}printf("cMd_Start:  %d\ncMd_End:    %d\nDifference: %d",cMd_time,GetTickCount(),GetTickCount()-cMd_time);
#endif

#define cMd:%0; forward cMd_%0(playerid, params[128], opt[128]); public cMd_%0(playerid, params[128], opt[128])


#if !defined sscanf
/*----------------------------------------------------------------------------*-
Function:
	sscanf
Params:
	string[] - String to extract parameters from.
	format[] - Parameter types to get.
	{Float,_}:... - Data return variables.
Return:
	0 - Successful, not 0 - fail.
Notes:
	A fail is either insufficient variables to store the data or insufficient
	data for the format string - excess data is disgarded.

	A string in the middle of the input data is extracted as a single word, a
	string at the end of the data collects all remaining text.

	The format codes are:

	c - A character.
	d, i - An integer.
	h, x - A hex number (e.g. a colour).
	f - A float.
	s - A string.
	z - An optional string.
	pX - An additional delimiter where X is another character.
	'' - Encloses a litteral string to locate.
	u - User, takes a name, part of a name or an id and returns the id if they're connected.

	Now has IsNumeric integrated into the code.

	Added additional delimiters in the form of all whitespace and an
	optioanlly specified one in the format string.
-*----------------------------------------------------------------------------*/

stock sscanf(string[], format[], {Float,_}:...)
{
#if defined isnull
	if (isnull(string))
#else
	if (string[0] == 0 || (string[0] == 1 && string[1] == 0))
#endif
	{
		return format[0];
	}
	new formatPos = 0, stringPos = 0, paramPos = 2, paramCount = numargs(), delim = ' ';
	while (string[stringPos] && string[stringPos] <= ' ')
	{
		stringPos++;
	}
	while (paramPos < paramCount && string[stringPos])
	{
		switch (format[formatPos++])
		{
			case EOS:
			{
				return 0;
			}
			case 'i', 'd':
			{
				new neg = 1, num = 0, ch = string[stringPos];
				if (ch == '-')
				{
					neg = -1;
					ch = string[++stringPos];
				}
				do
				{
					stringPos++;
					if ('0' <= ch <= '9')
					{
						num = (num * 10) + (ch - '0');
					}
					else
					{
						return -1;
					}
				}
				while ((ch = string[stringPos]) > ' ' && ch != delim);
				setarg(paramPos, 0, num * neg);
			}
			case 'h', 'x':
			{
				new num = 0, ch = string[stringPos];
				do
				{
					stringPos++;
					switch (ch)
					{
						case 'x', 'X':
						{
							num = 0;
							continue;
						}
						case '0' .. '9':
						{
							num = (num << 4) | (ch - '0');
						}
						case 'a' .. 'f':
						{
							num = (num << 4) | (ch - ('a' - 10));
						}
						case 'A' .. 'F':
						{
							num = (num << 4) | (ch - ('A' - 10));
						}
						default:
						{
							return -1;
						}
					}
				}
				while ((ch = string[stringPos]) > ' ' && ch != delim);
				setarg(paramPos, 0, num);
			}
			case 'c':
			{
				setarg(paramPos, 0, string[stringPos++]);
			}
			case 'f':
			{

				new changestr[16], changepos = 0, strpos = stringPos;
				while(changepos < 16 && string[strpos] && string[strpos] != delim)
				{
					changestr[changepos++] = string[strpos++];
 				}
				changestr[changepos] = EOS;
				setarg(paramPos,0,_:floatstr(changestr));
			}
			case 'p':
			{
				delim = format[formatPos++];
				continue;
			}
			case '\'':
			{
				new end = formatPos - 1, ch;
				while ((ch = format[++end]) && ch != '\'') {}
				if (!ch)
				{
					return -1;
				}
				format[end] = EOS;
				if ((ch = strfind(string, format[formatPos], false, stringPos)) == -1)
				{
					if (format[end + 1])
					{
						return -1;
					}
					return 0;
				}
				format[end] = '\'';
				stringPos = ch + (end - formatPos);
				formatPos = end + 1;
			}
			case 'u':
			{
				new end = stringPos - 1, id = 0, bool:num = true, ch;
				while ((ch = string[++end]) && ch != delim)
				{
					if (num)
					{
						if ('0' <= ch <= '9')
						{
							id = (id * 10) + (ch - '0');
						}
						else
						{
							num = false;
						}
					}
				}
				if (num && IsPlayerConnected(id))
				{
					setarg(paramPos, 0, id);
				}
				else
				{
					#if !defined foreach
						#define foreach(%1,%2) for (new %2 = 0; %2 < MAX_PLAYERS; %2++) if (IsPlayerConnected(%2))
						#define __SSCANF_FOREACH__
					#endif
					string[end] = EOS;
					num = false;
					new name[MAX_PLAYER_NAME];
					id = end - stringPos;
					foreach (Player, playerid)
					{
						GetPlayerName(playerid, name, sizeof (name));
						if (!strcmp(name, string[stringPos], true, id))
						{
							setarg(paramPos, 0, playerid);
							num = true;
							break;
						}
					}
					if (!num)
					{
						setarg(paramPos, 0, INVALID_PLAYER_ID);
					}
					string[end] = ch;
					#if defined __SSCANF_FOREACH__
						#undef foreach
						#undef __SSCANF_FOREACH__
					#endif
				}
				stringPos = end;
			}
			case 's', 'z':
			{
				new i = 0, ch;
				if (format[formatPos])
				{
					while ((ch = string[stringPos++]) && ch != delim)
					{
						setarg(paramPos, i++, ch);
					}
					if (!i)
					{
						return -1;
					}
				}
				else
				{
					while ((ch = string[stringPos++]))
					{
						setarg(paramPos, i++, ch);
					}
				}
				stringPos--;
				setarg(paramPos, i, EOS);
			}
			default:
			{
				continue;
			}
		}
		while (string[stringPos] && string[stringPos] != delim && string[stringPos] > ' ')
		{
			stringPos++;
		}
		while (string[stringPos] && (string[stringPos] == delim || string[stringPos] <= ' '))
		{
			stringPos++;
		}
		paramPos++;
	}
	do
	{
		if ((delim = format[formatPos++]) > ' ')
		{
			if (delim == '\'')
			{
				while ((delim = format[formatPos++]) && delim != '\'') {}
			}
			else if (delim != 'z')
			{
				return delim;
			}
		}
	}
	while (delim > ' ');
	return 0;
}

#endif
